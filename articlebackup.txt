{%comment%}<script src="{{ 'tinyslider.js' | asset_url }}" type="text/javascript" charset="utf-8"></script>{%endcomment%}
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="{{ jquery.fancybox.js | asset_url }}"></script>
{{ 'jquery.fancybox.css' | asset_url | stylesheet_tag }}

<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
		    // .attr('rel', 'gallery1')
		    // .fancybox({ 
				// padding: 0
				// 	maxWidth	: 800,
				// 	maxHeight	: 600,
				// 	fitToView	: true,
				// 	width		: '70%',
				// 	height		: '70%',
				// 	autoSize	: true,
				// 	closeClick	: true,
				// 	openEffect	: 'none',
				// 	closeEffect	: 'none'
		    // });
		// $('.fancybox').fancybox({
		// 	maxWidth	: 800,
		// 	maxHeight	: 600,
		// 	fitToView	: true,
		// 	width		: '70%',
		// 	height		: '70%',
		// 	autoSize	: true,
		// 	closeClick	: true,
		// 	openEffect	: 'none',
		// 	closeEffect	: 'none'
		// });
	});
</script>

{% paginate blog.articles by 5 %}



	<div class="sixteen columns" style="width: 960px; margin-left: 0; margin-right: 0; padding-left: 10px; padding-right: 10px; border-top: 1px solid #b5b5b5;">
		<div class="section clearfix">

			{%comment %}<h1><a href="{{ blog.url }}" title="{{ blog.title | escape }}">{{ blog.title }}</a></h1>{% endcomment %}

			<div class="breadcrumb">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ shop.url }}" title="{{ shop.name | escape }}" itemprop="url"><span itemprop="title">Home</span></a></span> 
				&#62;
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ blog.url }}" title="{{ blog.title | escape }}" itemprop="url"><span itemprop="title">{{ blog.title }}</span></a></span>       
				{% if current_tags %}
					{% for tag in current_tags %}
						&#62; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ shop.url}}/blogs/{{ blog.handle }}/tagged/{{ tag | handleize }}" title="{{ blog.title }} tagged {{ tag | escape }}" itemprop="url"><span itemprop="title">{{ tag }}</span></a></span>       
					{% endfor %}
				{% endif %}
				&#62; Page {{ paginate.current_page }} of {{ paginate.pages }}
			</div>
		</div>

		{% include "blog-sidebar" %}



		<div class="thirteen columns omega">
			{% for article in blog.articles %}

				<div class="three columns article slide">

					{% if article.excerpt != blank %}

						<div class="article-excerpt">
							{{ article.excerpt }}
						</div>
						<div class="article-content hide">

							<div id="slider{{ article.id }}" class="slider-container">
								<a class="buttons prev" href="#">left</a>
								<div class="viewport">
								<ul class="fancybox">{{ article.content }}</ul>{%endcomment%}
								</div>
								<a class="buttons next" href="#">right</a>
							</div>


						</div>

					{% endif %}
					

					<h3><a href="{{ article.url }}" title="{{ article.title | escape }}">{{ article.title }}</a></h3>
					<p class="meta">
						<span class="label">Posted on</span>  {{ article.published_at | date: "%B %d, %Y" }}
						<span class="label">by</span> {{ article.author }}
						{% if article.comments_enabled? %} | <a href="{{ article.url }}#comments" title="{{ article.title | escape }} Comments">{{ article.comments_count }} {{ article.comments_count | pluralize: 'Comment', 'Comments' }} </a>{% endif %}
					</p>

					{% if article.excerpt != blank %}
						<p class="continue_reading"><a href="{{ article.url }}" title="{{ article.title | escape }}">Continue Reading &rarr;</a></p>
					{% else %}
						{{ article.content }}
					{% endif %}
				
					{% include "social-buttons" with "article" %}

					<p class="meta">
						{% for tag in article.tags %}
							{% if forloop.index0 == 0 %}
								<span class="label">Posted in</span>
							{% endif %}
							<a href="{{ shop.url}}/blogs/{{ blog.handle }}/tagged/{{ tag | handleize }}" title="{{ blog.title }} tagged {{ tag | escape }}">{{ tag }}</a>{% unless forloop.last %},{% endunless %}
						{% endfor %}
					</p>

				</div>

			{% endfor %}
{% endcomment %}
			{% include 'pagination' %}

		</div>
	</div>
{% endpaginate %}
<div class="article-overlay hide"></div>
{%comment%}
<script src="{{ 'tinyslider.js' | asset_url }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
	{% for article in blog.articles %}
		$(document).ready(function(){ $('#slider{{ article.id }}').tinycarousel({ duration: 500 }); });
	{% endfor%}
</script>
{%endcomment%}